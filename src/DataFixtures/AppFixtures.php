<?php

namespace App\DataFixtures;

use App\Entity\Tienda;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for($i = 0; $i<5; $i++){
            $articulo = new Tienda();
            $articulo->setNombre("Articulo ".strval($i));
            $articulo->setDescripcion("Una pequeña descripcion de mi articulo ".strval($i));
            $articulo->setPrecio(random_int(1,50));
            $manager->persist($articulo);
            $manager->flush();
        }

        
    }
}
