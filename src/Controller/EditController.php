<?php

namespace App\Controller;
use App\Form\TiendaFormType;
use App\Entity\Tienda;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
class EditController extends AbstractController
{
    /**
     * @Route("/edit/{id}", name="app_edit")
     */
    public function index(Tienda $tienda, Request $request, ManagerRegistry $doctrine): Response
    {   

        $form = $this->createForm(TiendaFormType::class, $tienda);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $articulo = $form->getData();
            $em = $doctrine->getManager();
            $em->persist($articulo);
            $em->flush();
            return $this->redirectToRoute('app_list');
        };
        return $this->render('create/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
